import flask

flask_app = flask.Flask(__name__) 

@flask_app.errorhandler(404)
def page_not_found(e):
    return str("<h1>Not Found<h1>")

# api.init_app(flask_app)
def runApp():
    # serve(flask_app, listen='0.0.0.0:5001')
    flask_app.run(host='0.0.0.0', port=5001)
