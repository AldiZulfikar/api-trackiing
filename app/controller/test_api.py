from flask_restx import Resource, request, jsonify
from model.model import APILog, ApiLogSchema
from service.namespace import api
from database.DBEngine import db
from .flaskinstance import runApp
import time

class MyApi(Resource):
    def example_api():
        start_time = time.time()
        request_body = request.json
        # Lakukan sesuatu dengan request body
        response_body = {'message': 'OK'}
        response_time = time.time() - start_time
        status_code = 200
        log = APILog(
            endpoint=request.path,
            method=request.method,
            request_body=str(request_body),
            response_body=str(response_body),
            status_code=status_code,
            response_time=response_time
        )
        db.session.add(log)
        db.session.commit()
        return jsonify(response_body), status_code

class get_logs(Resource):
    def get():
        logs = APILog.query.all()
        log_schema = ApiLogSchema(many=True)
        result = log_schema.dump(logs)
        return jsonify(result)