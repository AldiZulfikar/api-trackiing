from app.controller import flaskinstance

def main():
    instance = flaskinstance
    instance.runApp()

if __name__ == '__main__':
    main()
