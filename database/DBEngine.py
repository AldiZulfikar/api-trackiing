import sqlalchemy as sqa
from app.controller.flaskinstance import flask_app
from flask_marshmallow import Marshmallow

db = sqa(flask_app)
ma = Marshmallow(flask_app)

# psgrsql_connection_string = 'postgresql+psycopg2://{username}:{password}@{hostname}:{port}/{service_name}'
mysql_connection_string = 'mysql://aldi:aldi123@localhost/api_tracking'

audit_log_api = sqa.create_engine(
			mysql_connection_string.format(
				username='aldi',
				password='aldi123',
				hostname='localhost',
				port='3306',
				service_name='api_tracking'
				)
		)
db_log_api = sqa.SQLAlchemy(audit_log_api)
