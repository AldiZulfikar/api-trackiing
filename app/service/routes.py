from app.service.namespace import api, audit_api

from app.controller import test_api

audit_api.add_resource(test_api.get_logs, '/logs')

api.add_resource(test_api.MyApi, '/example')