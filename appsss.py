from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from marshmallow_sqlalchemy import ModelSchema
import time

# Inisialisasi aplikasi Flask
app = Flask(__name__)

# Konfigurasi aplikasi Flask
app.config.from_object('config.DevelopmentConfig')

# Inisialisasi database dan Marshmallow
db = SQLAlchemy(app)
ma = Marshmallow(app)

# Model untuk tabel log
class APILog(db.Model):
    __tablename__ = 'api_log'
    id = db.Column(db.Integer, primary_key=True)
    endpoint = db.Column(db.String(255), nullable=False)
    method = db.Column(db.String(10), nullable=False)
    request_body = db.Column(db.Text)
    response_body = db.Column(db.Text)
    status_code = db.Column(db.Integer, nullable=False)
    response_time = db.Column(db.Float, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=db.func.now())

# Schema untuk model log
class APILogSchema(ModelSchema):
    class Meta:
        model = APILog

# Endpoint untuk contoh API
@app.route('/example', methods=['POST'])
def example_api():
    start_time = time.time()
    request_body = request.json
    # Lakukan sesuatu dengan request body
    response_body = {'message': 'OK'}
    response_time = time.time() - start_time
    status_code = 200
    log = APILog(
        endpoint=request.path,
        method=request.method,
        request_body=str(request_body),
        response_body=str(response_body),
        status_code=status_code,
        response_time=response_time
    )
    db.session.add(log)
    db.session.commit()
    return jsonify(response_body), status_code

# Endpoint untuk mendapatkan log
@app.route('/logs', methods=['GET'])
def get_logs():
    logs = APILog.query.all()
    log_schema = APILogSchema(many=True)
    result = log_schema.dump(logs)
    return jsonify(result)

if __name__ == '__main__':
    app.run()