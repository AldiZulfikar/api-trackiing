from flask_restx import Namespace

api = Namespace('my_api', description='test endpoint')

audit_api = Namespace('audit_api', description='Audit log penggunaan API')