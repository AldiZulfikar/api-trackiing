from app.service.namespace import audit_api
from flask_restx import fields
from marshmallow_sqlalchemy import ModelSchema
from database.DBEngine import db
from sqlalchemy import Column, Integer, String, DateTime
from datetime import datetime

# Model untuk tabel log
class APILog(db.Model):
    __tablename__ = 'api_log'

    id = Column(Integer, primary_key=True)
    method = Column(String(10), nullable=False)
    url = Column(String(100), nullable=False)
    request_body = Column(String(1000))
    response_body = Column(String(1000))
    status_code = Column(Integer)
    response_time = Column(Integer)
    created_at = Column(DateTime, default=datetime.now())

# Schema untuk model log
class ApiLogSchema(ModelSchema):
    class Meta:
        model = APILog

    created_at = fields.DateTime('%Y-%m-%d %H:%M:%S')

log_api_model = audit_api.model('Log API', {
    'method': fields.String(required=True, description='HTTP method'),
    'url': fields.String(required=True, description='API endpoint URL'),
    'request_body': fields.String(description='Request body'),
    'response_body': fields.String(description='Response body'),
    'status_code': fields.Integer(description='Response status code'),
    'response_time': fields.Integer(description='Response time in milliseconds')
})